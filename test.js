var I2C_RELAY = require('./index.js');

var sensor = I2C_RELAY({address: 0x47});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

//sensor.scan();

sensor.set(); 
sleep(1000);
sensor.reset();

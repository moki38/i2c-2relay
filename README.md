# index.js 

Access a Bitwizard 2Relay i2c board on your Raspberry Pi using Node.js.

### Install

```
$ npm install i2c-2relay
```

### Usage

var I2C_RELAY = require('./index.js');

var sensor = I2C_RELAY({address: 0x47});

sensor.set();

sensor.reset();

```

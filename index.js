var async = require('async');
var i2c = require('i2c');

module.exports = function I2C_RELAY(options) {
  var sensor = function() {};
  var wire = new i2c(options.address);

  var RELAY_ALL	= 0x10;
  var RELAY0	= 0x20;
  var RELAY1 	= 0x21;
  var RELAY_OFF	= 0x00; 
  var RELAY_ON	= 0xff; 
 
  sensor.scan = function () {
    wire.scan(function (err, data) {
      data.forEach(function (item) {
        console.log(item);
      });
    });
  };

  sensor.reset = function () {
    wire.writeBytes(RELAY0, [RELAY_OFF]) ;
  };

  sensor.set = function (call) {
    wire.writeBytes(RELAY0, [RELAY_ON]);
  };

  return sensor;
};
